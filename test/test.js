const main = require("../app.js");
const assert = require("assert");
describe("Call Function", function () {
  describe("without arguements", function () {
    it("should return message when the values is not present", async function () {
      assert.equal(await main(), "You don't have any input arguments");
    });
  });
  describe("with searchString = 37", function () {
    it("should return 3 when the value is 37", async function () {
      const { count } = await main("37", "table1");
      assert.equal(count, 3);
    });
  });
  describe("with searchString = test", function () {
    it("should return true when the value is test", async function () {
      const { data, count } = await main("test", "table1");
      assert.equal(count > data.length, true);
    });
  });
  describe('with searchString = "This search string should never exist"', function () {
    it('should return 0 when the value is "This search string should never exist"', async function () {
      const { count } = await main(
        "This search string should never exist",
        "table1"
      );
      assert.equal(count, 0);
    });
  });
  describe("with searchString = 40", function () {
    it("should return true when the value is 40", async function () {
      const data = ["а", "б", "в", "г", "д"];
      const { data: rows } = await main("40", "table1");
      const result = rows.map((item) => item.name[0]);
      assert.equal(JSON.stringify(result) === JSON.stringify(data), true);
    });
  });
  describe('with searchString = 40/"40"', function () {
    it('should return true when the value is 40 and "40"', async function () {
      const { data: rows_with_string } = await main("40", "table1");
      const { data: rows_with_int } = await main(40, "table1");
      assert.equal(
        JSON.stringify(rows_with_int) === JSON.stringify(rows_with_string),
        true
      );
    });
  });
});
