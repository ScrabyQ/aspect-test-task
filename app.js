const express = require("express");
const app = express();
const mysql2 = require("mysql2/promise");

const main = async function (searhString = null, database = null) {
  const data = Array();
  const connection = await mysql2.createConnection({
    host: "localhost",
    user: "root",
    database: "db",
    password: "1234",
  });
  const query = `select * from ${database} 
    where name like "%${searhString}%" or 
    description like "%${searhString}%"
    order by name`;
  try {
    const [rows] = await connection.execute(query);

    for (let i = 0; i <= 19; ++i) {
      rows[i] !== undefined ? data.push(await rows[i]) : "";
    }
    return { data, count: rows.length };
  } catch (e) {
    if (e.code === "ER_PARSE_ERROR")
      return "You don't have any input arguments";
  }
};
app.listen(1888, () => {
  console.log("server starting");
  main("test", "table1").then((data) => {
    console.log(data);
  });
});

module.exports = main;
